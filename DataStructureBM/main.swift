//
//  main.swift
//  DataStructureBM
//
//  Created by Jeremy Pereira on 06/08/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

import Foundation

func orderAndDelete(count: Int, container: OrderedList)
{
    assert(count <= Int(UInt32.max))
    
    for _ in 0 ..< count
    {
        let value = Int(arc4random_uniform(UInt32(count)))
		container.insert(element: value)
    }
//    print(type(of: container))
//    print(container)
//    assert(container.count == container.toArray().count, "Count inconsistency")
	while(container.count > 0)
    {
        let index = arc4random_uniform(UInt32(container.count))
        container.delete(index: Int(index))
    }
}


func evaluate(count: Int, block: (Int) -> ()) -> Double
{
    let start = DispatchTime.now() // <<<<<<<<<< Start time
    block(count)
    let end = DispatchTime.now()   // <<<<<<<<<<   end time
    let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds // <<<<< Difference in nano seconds (UInt64)
    let timeInterval = Double(nanoTime) / 1_000_000_000 // Technically could overflow for long running tests
    return timeInterval
}

for iterations in  [10, 100, 1000, 10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000]
{
//    let listTime = evaluate(count: iterations)
//    {
//        orderAndDelete(count: $0, container: ListOrderedList())
//    }
    let arrayTime = evaluate(count: iterations)
    {
        orderAndDelete(count: $0, container: ArrayOrderedList())
    }
    let unbalancedTreeTime = evaluate(count: iterations)
    {
        orderAndDelete(count: $0, container: UnbalancedBinaryTree())
    }
	print("\(iterations)\t\(arrayTime)\t\(unbalancedTreeTime)")
}
