//
//  OrderedList.swift
//  DataStructureBM
//
//  Created by Jeremy Pereira on 06/08/2017.
//  Copyright © 2017 Jeremy Pereira. All rights reserved.
//

protocol OrderedList: CustomStringConvertible
{
    func insert(element: Int)

    func delete(index: Int)

    var count: Int { get }

    func toArray() -> [Int]
}

extension OrderedList
{
    var description: String { return self.toArray().description }
}

class ArrayOrderedList: OrderedList
{
    private var store: [Int] = []

    func insert(element: Int)
    {
        var index = 0
        while index < store.count && store[index] < element
        {
            index += 1
        }
        if index < store.count
        {
            store.insert(element, at: index)
        }
        else
        {
            store.append(element)
        }
    }

    func delete(index: Int)
    {
        store.remove(at: index)
    }
    var count: Int { return store.count }

    func toArray() -> [Int]
    {
        return store
    }
}

class ListOrderedList: OrderedList
{
    private class Item
    {
        static var nextId = 0
//        var id: Int
        weak var prev: Item?
        var next: Item?
        let value: Int

        init(value: Int)
        {
            self.value = value
        }

//        deinit
//        {
//            print("Deleting Item \(id)")
//        }
    }

    private var head: Item?
    private weak var tail: Item?
    private var _count: Int = 0
    var count: Int { return _count }

    func insert(element: Int)
    {
        guard let head = head else
        {
            append(item: Item(value: element))
            return
        }

        var currentItem: Item? = head
        while let testItem = currentItem
        {
            guard testItem.value < element else { break }
            currentItem = testItem.next
        }
		if let currentItem = currentItem
        {
            insertBefore(currentItem, item: Item(value: element))
        }
        else
        {
            append(item: Item(value: element))
        }
    }

    func delete(index: Int)
    {
        precondition(index < count, "Index out of bounds")
        var currentItem = head!
        for _ in 0 ..< index
        {
            currentItem = currentItem.next!
        }
		if let prevItem = currentItem.prev
        {
            prevItem.next = currentItem.next
        }
        else
        {
			head = currentItem.next
        }
        if let nextItem = currentItem.next
        {
            nextItem.prev = currentItem.prev
        }
        else
        {
            tail = currentItem.prev
        }
        currentItem.next = nil
        currentItem.prev = nil
        _count -= 1
    }

    private func insertBefore(_ existing: Item, item: Item)
    {
        if let existingPrev = existing.prev
        {
            existingPrev.next = item
            item.prev = existingPrev
        }
        else
    	{
			head = item
        }
        existing.prev = item
        item.next = existing
        _count += 1
    }

    private func append(item: Item)
    {
        item.prev = tail
       	if let tail = tail
        {
            tail.next = item
        }
        else
        {
            assert(head == nil, "If tail is nil so should be head")
            head = item
        }
        tail = item
        _count += 1
    }

    func toArray() -> [Int]
    {
        var ret: [Int] = []

        var currentItem = head
        while let testItem = currentItem
        {
            ret.append(testItem.value)
            currentItem = testItem.next
        }
        return ret
    }
}

class UnbalancedBinaryTree: OrderedList
{
    var count: Int { return root?.count ?? 0 }

    func delete(index: Int)
    {
        precondition(index >= 0 && index < count)

        guard let root = root else { return }

        var deleted = false
		var currentItem = root
        var subIndex = index
        while !deleted
        {
			let leftCount = currentItem.left?.count ?? 0
            if leftCount > subIndex
            {
                currentItem = currentItem.left!
            }
            else if leftCount < subIndex
            {
                subIndex -= leftCount + 1
                currentItem = currentItem.right!
            }
            else
            {
                removeFromTree(item: currentItem)
                deleted = true
            }
        }
    }

    private func removeFromTree(item: Item)
    {
        let left = item.left
        let right = item.right
        item.left = nil
        item.right = nil
        switch (left, right)
        {
        case (let left?, nil):
            if let parent = item.parent
            {
                parent.replace(child: item, with: left)
             }
            else
            {
                root = left
                left.parent = nil
            }
        case (nil, let right?):
            if let parent = item.parent
            {
                parent.replace(child: item, with: right)
            }
            else
            {
                root = right
                right.parent = nil
            }
        case (let left?, let right?):
            // We have the choice to take the right sub tree and add it to the
            // end of the left sub tree or take the left sub tree and add it to
            // the end of the right sub tree. We will always take the shorter
            // of the too or the tree will degenerate into a list fairly qwuickly.
            let leftCandidate = left.findLastItem()
            let rightCandidate: (Item, Int)?
            if leftCandidate.1 > 0 // No point in checking right if left id already as short as possible
            {
				rightCandidate = right.findFirstItem()
            }
            else
            {
                rightCandidate = nil
            }
            if rightCandidate == nil || leftCandidate.1 < rightCandidate!.1
            {
				leftCandidate.0.right = right
        		if let parent = item.parent
                {
                    parent.replace(child: item, with: leftCandidate.0)
                }
                else
                {
                    root = leftCandidate.0
                }
            }
            else
            {
				rightCandidate!.0.left = left
            }

        case (nil, nil):
            if let parent = item.parent
            {
                parent.remove(child: item)
            }
            else
            {
                root = nil
            }
        }
    }

    func insert(element: Int)
    {
        guard let root = root else { self.root = Item(value: element) ; return }

		var currentItem = root
        var inserted = false
        while !inserted
        {
            if currentItem.value > element
            {
                if let left = currentItem.left
                {
                    currentItem = left
                }
                else
                {
                    let newItem = Item(value: element)
                    currentItem.left = newItem
                    inserted = true
                }
            }
            else
            {
                if let right = currentItem.right
                {
                    currentItem = right
                }
                else
                {
                    let newItem = Item(value: element)
                    currentItem.right = newItem
                    inserted = true
                }
            }
        }
        //print("Inserted \(element) below \(currentItem)")
    }

    fileprivate class Item
    {
        var left: Item?
        {
            willSet
            {
                if let left = left
                {
                    left.parent = nil
                    adjustCount(-left.count)
                }
            }
            didSet
            {
                if let left = left
                {
                    left.parent = self
                   adjustCount(left.count)
                }
            }
        }

        var right: Item?
        {
            willSet
            {
                if let right = right
                {
                    right.parent = nil
                    adjustCount(-right.count)
                }
            }
            didSet
            {
                if let right = right
                {
                    right.parent = self
                    adjustCount(right.count)
                }
            }
        }

        weak var parent: Item?
        let value: Int
        var count: Int

        init(value: Int)
        {
            self.value = value
            self.count = 1
        }

        private func adjustCount(_ delta: Int)
        {
            var item: Item? = self
            while let currentItem = item
            {
                currentItem.count += delta
                item = currentItem.parent
            }
        }

        fileprivate func remove(child: Item)
        {
            if child === left
            {
                left = nil
            }
            else if child === right
            {
                right = nil
            }
        }

        fileprivate func replace(child: Item, with replacement: Item)
        {
            if child === left
            {
                left = replacement
            }
            else if child === right
            {
                right = replacement
            }
        }

        fileprivate func findLastItem() -> (Item, Int)
        {
            var depth: Int = 0
            var ret = self
            var currentItem = right
            while let testItem = currentItem
            {
                ret = testItem
                depth += 1
                currentItem = testItem.right
            }
            return (ret, depth)
        }

        fileprivate func findFirstItem() -> (Item, Int)
        {
            var depth: Int = 0
            var ret = self
            var currentItem = left
            while let testItem = currentItem
            {
                ret = testItem
                depth += 1
                currentItem = testItem.left
            }
            return (ret, depth)
        }

        func toArray() -> [Int]
        {
            var ret = [value]
            if let left = left
            {
                ret = left.toArray() + ret
            }
            if let right = right
            {
                ret = ret + right.toArray()
            }
            return ret
        }
   	}

    private var root: Item?

    func toArray() -> [Int]
    {
        guard let root = root else { return [] }

        return root.toArray()
    }
}

extension UnbalancedBinaryTree.Item: CustomStringConvertible
{
    var description: String
    {
        guard let parent = parent else { return "root(\(value))" }

        if parent.left === self
        {
            return parent.description + ".left(\(value))"
        }
        else if parent.right === self
        {
            return parent.description + ".right(\(value))"
        }
        else
        {
            fatalError("item not in its own parent")
        }
    }
}


